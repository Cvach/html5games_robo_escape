var box2d = {
	b2Vec2 : Box2D.Common.Math.b2Vec2,
	b2BodyDef : Box2D.Dynamics.b2BodyDef,
	b2Body : Box2D.Dynamics.b2Body,
	b2FixtureDef : Box2D.Dynamics.b2FixtureDef,
	b2Fixture : Box2D.Dynamics.b2Fixture,
	b2World : Box2D.Dynamics.b2World,
	b2MassData : Box2D.Collision.Shapes.b2MassData,
	b2PolygonShape : Box2D.Collision.Shapes.b2PolygonShape,
	b2CircleShape : Box2D.Collision.Shapes.b2CircleShape,
	b2DebugDraw : Box2D.Dynamics.b2DebugDraw,
    b2ContactListener : Box2D.Dynamics.b2ContactListener,
    b2Joints: Box2D.Dynamics.Joints;
};
var scale = 30;

var world;
var context;
var actors =[];


function init() {
    world = new box2d.b2World(new box2d.b2Vec2(0, 9.80),true);      
    
    var canvas = document.getElementById("game");
    canvas.width = 800;
    canvas.height = 600;
    var debugDraw = new box2d.b2DebugDraw();

    context = document.getElementById("debug").getContext('2d');
    
    debugDraw.SetSprite(context);
    debugDraw.SetDrawScale(scale);
    debugDraw.SetFillAlpha(0.5);
    debugDraw.SetFlags(box2d.b2DebugDraw.e_shapeBit | box2d.b2DebugDraw.e_jointBit);
    world.SetDebugDraw(debugDraw);
    
    var listener = new box2d.b2ContactListener();
//    console.log(listener);
    listener.PostSolve = function(contact, impulse) {
         var bodyA = contact.GetFixtureA().GetBody().GetUserData();
           var bodyB = contact.GetFixtureB().GetBody().GetUserData();
        
//        console.log(bodyB.contact);
        if (bodyA.contact) {
            bodyA.contact(bodyB, impulse, true)
        }
        if (bodyB.contact) {
            bodyB.contact(bodyA, impulse, false)
        }
    }
    
    world.SetContactListener(listener);
   
}

 function Actor(width,height,pX,pY,type, img){
        this.bodyImg = img;
        this.bodyImg.regY += img.getBounds().height / 2 ;
        this.bodyImg.regX += img.getBounds().width / 2;
        
        var fixDef = new Box2D.Dynamics.b2FixtureDef;
        fixDef.density = 1.0;
        fixDef.friction = 0.5;
        fixDef.restitution = 0.2;
      
        var bodyDef = new box2d.b2BodyDef;
        bodyDef.type = type;

	 	bodyDef.position.x = pX / scale;
        bodyDef.position.y = pY / scale;
        fixDef.shape = new box2d.b2PolygonShape();
     
       fixDef.shape.SetAsBox((width / scale) / 2, height / scale / 2);
       this.bodyImg.body = world.CreateBody(bodyDef);
        this.bodyImg.body.SetUserData(this);
       this.bodyImg.body.CreateFixture(fixDef);
        
      this.update = function () {
            this.bodyImg.x = this.bodyImg.body.GetPosition().x * scale;
            this.bodyImg.y = this.bodyImg.body.GetPosition().y * scale;
            this.rotation = this.bodyImg.body.GetAngle() * (180 / Math.PI);
      }
 }

      
      
function meSensor(width,height,pX,pY,type, img)
{
        this.bodyImg = img;
        this.bodyImg.regY += img.getBounds().height/2 ;
        this.bodyImg.regX += img.getBounds().width / 2;
    
        
        var fixDef = new Box2D.Dynamics.b2FixtureDef;
        fixDef.density = 1.0;
        fixDef.friction = 0.5;
        fixDef.restitution = 0.2;
      
        var bodyDef = new box2d.b2BodyDef;
        bodyDef.type = type;

	 	bodyDef.position.x = pX / scale;
        bodyDef.position.y = pY / scale;
        fixDef.shape = new box2d.b2PolygonShape();
     
       fixDef.shape.SetAsBox((width / scale) / 2, (height / scale) / 2);
       this.bodyImg.body = world.CreateBody(bodyDef);
        this.bodyImg.body.SetUserData(this);
//    fixDef.isSensor=true;
       this.bodyImg.body.CreateFixture(fixDef);
        

        
      this.update = function () {
            this.bodyImg.x = this.bodyImg.body.GetPosition().x * scale;
            this.bodyImg.y = this.bodyImg.body.GetPosition().y * scale;
            this.rotation = this.bodyImg.body.GetAngle() * (180 / Math.PI);
      }
}

 function NonRotatingActor(width,height,pX,pY,type, img){
       this.bodyImg = img;
        this.bodyImg.regY += img.getBounds().height/2-30;
        this.bodyImg.regX +=img.getBounds().width/2;
     
       var fixDef = new Box2D.Dynamics.b2FixtureDef;
       fixDef.density = 1.5;
       fixDef.friction = 0.3;
       fixDef.restitution = 0.2;
     
     
       var bodyDef = new box2d.b2BodyDef;

       bodyDef.type = type;
       bodyDef.fixedRotation = true;
       bodyDef.position.x = pX / scale;
       bodyDef.position.y = pY / scale;
       fixDef.shape = new box2d.b2PolygonShape();
     
       fixDef.shape.SetAsBox((width / scale) / 2, (height)/ scale /2);

       this.bodyImg.body = world.CreateBody(bodyDef);
       this.bodyImg.body.SetUserData(this);

       this.bodyImg.body.CreateFixture(fixDef);

      this.update = function () {
            this.bodyImg.x = this.bodyImg.body.GetPosition().x * scale;
            this.bodyImg.y = this.bodyImg.body.GetPosition().y * scale;
            this.rotation = this.bodyImg.body.GetAngle() * (180 / Math.PI);
      }    
      
      this.contact = function(contact, impulse, first) {
               if(contact.blockType == blockType.SPIKE_BLOCK) {
                    console.log("die");   
               }

      }
          
}

function updatePhysics(dt) {
    
    world.Step(dt,10, 10);

    for(i = 0; i < actors.length;  i++) {
            actors[i].update();
    }
    
    world.DrawDebugData();
    world.ClearForces();

    
}














































































































































































