﻿(function(window) {
punchBlocks_instance_1 = function() {
	this.initialize();
}
punchBlocks_instance_1._SpriteSheet = new createjs.SpriteSheet({images: ["punchBlock.png"], frames: [[0,0,160,297,0,37,140.4],[160,0,160,297,0,37,140.4],[320,0,160,297,0,37,140.4],[0,297,160,297,0,37,140.4],[160,297,160,297,0,37,140.4],[320,297,160,297,0,37,140.4],[0,297,160,297,0,37,140.4],[160,297,160,297,0,37,140.4],[320,297,160,297,0,37,140.4],[480,297,160,297,0,37,140.4],[640,297,160,297,0,37,140.4],[800,297,160,297,0,37,140.4],[0,594,160,297,0,37,140.4],[160,594,160,297,0,37,140.4],[320,594,160,297,0,37,140.4],[480,594,160,297,0,37,140.4]]});
var punchBlocks_instance_1_p = punchBlocks_instance_1.prototype = new createjs.Sprite();
punchBlocks_instance_1_p.Sprite_initialize = punchBlocks_instance_1_p.initialize;
punchBlocks_instance_1_p.initialize = function() {
	this.Sprite_initialize(punchBlocks_instance_1._SpriteSheet);
	this.paused = false;
}
window.punchBlocks_instance_1 = punchBlocks_instance_1;
}(window));

