﻿(function(window) {
spikeMoving_instance_1 = function() {
	this.initialize();
}
spikeMoving_instance_1._SpriteSheet = new createjs.SpriteSheet({images: ["spikeBlock.png"], frames: [[0,0,88,252,0,63,231.1],[88,0,88,252,0,63,231.1],[176,0,88,252,0,63,231.1],[264,0,88,252,0,63,231.1],[352,0,88,252,0,63,231.1],[440,0,88,252,0,63,231.1],[528,0,88,252,0,63,231.1],[616,0,88,252,0,63,231.1],[704,0,88,252,0,63,231.1],[792,0,88,252,0,63,231.1],[880,0,88,252,0,63,231.1],[0,252,88,252,0,63,231.1],[88,252,88,252,0,63,231.1],[176,252,88,252,0,63,231.1],[264,252,88,252,0,63,231.1],[352,252,88,252,0,63,231.1]]});
var spikeMoving_instance_1_p = spikeMoving_instance_1.prototype = new createjs.Sprite();
spikeMoving_instance_1_p.Sprite_initialize = spikeMoving_instance_1_p.initialize;
spikeMoving_instance_1_p.initialize = function() {
	this.Sprite_initialize(spikeMoving_instance_1._SpriteSheet);
	this.paused = false;
}
window.spikeMoving_instance_1 = spikeMoving_instance_1;
}(window));

