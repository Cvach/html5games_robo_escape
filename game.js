var queue;
var hitCount = 3;
var hitOffset = 30;
var runningDistance = 0;
var jamieModeOn = false;
var haveIGottenHereYet = false;

var meTriggers = [];
var meGates = []; 
    
var currentLevel = 1;
//creates game st ates
var gameStates = {
    MAIN_MENU:0, 
    GAME_SCENE:1,
    GAME_OVER:2,
    GAME_INSTRUCTION:3,
	LEVEL_COMPLETE:4,
    CREDITS:5,
}

var gameStateEvents = {
    MAIN_MENU:'main-menu-event',
    GAME:'game-event',
    GAME_OVER:'game-over-event'
}

var keyStates = {
    VK_ARROW_UP:    false,
    VK_ARROW_LEFT:  false,
    VK_ARROW_RIGHT: false,    
    VK_ARROW_DOWN:  false,
    VK_SPACE: false,

}

var levelStates = {
    LEVEL_ONE:1, 
    LEVEL_TWO:2,    
    LEVEL_THREE:3,    
    LEVEL_FOUR:4,    
    LEVEL_FIVE:5,    
    LEVEL_SIX:6,    

}

var currentGameState = gameStates.MAIN_MENU;
var currentGameFunction;
var currentGameScene;

function changeGameState(state) {
    currentGameState = state;
    console.log("changing game state");

	levelContainer.removeAllChildren();
    stage.removeAllChildren();
    actors.length = 0;        
    portalSet.length = 0;
    meTriggers.length = 0;
    currentGameFunction = null;
	player.alive = true;
    makeButtons();
    
    switch(currentGameState) {
        case gameStates.MAIN_MENU:
            console.log("changing to main menu");
            
            currentGameFunction = gameStateMainMenu;
            break;
        case gameStates.GAME_SCENE:
            console.log("changing to level one");
            currentGameFunction = loadLevel;
            break;
			
        case gameStates.GAME_OVER:
            console.log("swithing to game over menu");
            currentGameFunction = gameStateGameOver;
            break;              
        case gameStates.GAME_INSTRUCTION:
            currentGameFunction = gameStateInstruction;
            break;
		case gameStates.LEVEL_COMPLETE:
            currentGameFunction = gameStateLevelComplete;
            break;
        case gameStates.CREDITS:
            currentGameFunction = gameStateCredits;

            break;
    }
    
	if(currentGameState == gameStates.GAME_SCENE){
		currentGameFunction(currentLevel);
	}else{
		currentGameFunction();
	}
}


function loadLevel(level) {
    currentGameState = gameStates.GAME_SCENE;
    console.log("loading level");

	levelContainer.removeAllChildren();
    actors.length = 0;        
    portalSet.length = 0;
    meTriggers.length = 0;
	currentGameScene.removeAllChildren();
	player.alive = true;

    stage.removeAllChildren();
    switch(level) {
        case levelStates.LEVEL_ONE:
            console.log("prepare to load level one");
            
            currentGameFunction = gameLevelOne;
            break;
        case levelStates.LEVEL_TWO:
            console.log("prepare to load level two");
            currentGameFunction = gameLevelTwo;

            break;
        case levelStates.LEVEL_THREE:
            console.log("prepare to load level three");
           
            currentGameFunction = gameLevelThree;
            break;              
        case levelStates.LEVEL_FOUR:
            console.log("prepare to load level four");
			
            currentGameFunction = gameLevelFour;
            break;
		case levelStates.LEVEL_FIVE:
            console.log("prepare to load level five");

            currentGameFunction = gameLevelFive;
            break;
        case levelStates.LEVEL_SIX:
            console.log("prepare to load level six");

            currentGameFunction = gameLevelSix;
            break;
    }
	currentGameFunction();
}


var AssetManager = {     
     //graphics
    GAME_SPRITES: "game_sprites",
    
    //sound
    BACKGROUND_MUSIC: "BACKGROUND_MUSIC",
    
    //data
    IMG_PATH: "assets/img/",
    SOUND_PATH: "assets/audio/",
    SPRITE_PATH: "assets/spriteSheet/",   
	NEW_SPRITE_PATH: "assets/img/Sprites/",
	PLATFORM_PATH: "assets/img/platforms/",
    manifest: null,
    loadProgress: 0,
    
    //events
    
    AssetManager: function() {
        this.prototype = new createjs.EventDispatcher();
        this.manifest = [
            {src:this.SOUND_PATH + "GameSounds/Purple_Nurple.mp3", id:"mainBGM"},
            {src: this.SOUND_PATH + "GameSounds/Grapple.mp3", id:"shoot"},
            {src: this.SOUND_PATH + "GameSounds/Fizzle.mp3", id:"switchActivate"},
            {src: this.SOUND_PATH + "slide.ogg", id:"slide"},

            {src: this.IMG_PATH + "bg.jpg", id:"bg"},
//            {src: this.IMG_PATH + "jumpBlock.bmp", id:"jumpBlock"},
            {src: this.SPRITE_PATH + "spikeBlock.png", id:"spikeBlock"},            
            {src: this.IMG_PATH + "titleImg.png", id:"titleImg"},
            {src: this.IMG_PATH + "gameOverImg.jpg", id:"gameOverImg"},
            {src: this.IMG_PATH + "instructions.jpg", id:"instructionImg"},
            {src: this.IMG_PATH + "gate.jpg", id:"gate"},
            {src: this.IMG_PATH + "goal.png", id:"goal"},
            {src: this.IMG_PATH + "powerUp.png", id:"powerUp"},
            {src: this.IMG_PATH + "trap.png", id:"trap"},
            {src: this.IMG_PATH + "LevelComplete.jpg", id:"levelComplete"},
            {src: this.IMG_PATH + "CreditScreen.jpg", id:"credits"},

//            {src: this.IMG_PATH + "trigger.png", id:"trigger"},
//            {src: this.IMG_PATH + "fan.bmp", id:"fan"},            
            {src: this.IMG_PATH + "portal_blue.jpg", id:"portalBlue"},
            {src: this.IMG_PATH + "portal_red.jpg", id:"portalRed"},

			
			{src: this.NEW_SPRITE_PATH + "Player/Player_Spritesheet.png", id:"player"},
			{src: this.NEW_SPRITE_PATH + "Grapple/Grapple.png", id:"grapple"},
			{src: this.NEW_SPRITE_PATH + "Jumpblock/Jumpblock_Spritesheet.png", id:"jumpblock2"},
			{src: this.NEW_SPRITE_PATH + "Traps/Fan/Fan_Spritesheet.png", id:"fan"},
			{src: this.NEW_SPRITE_PATH + "Traps/Spike/Spike_Spritesheet.png", id:"spike"},
			{src: this.NEW_SPRITE_PATH + "Trigger/Trigger_Spritesheet.png", id:"trigger"},
			{src: this.NEW_SPRITE_PATH + "Traps/Fan/windH.png", id:"windHorizontal"},      
//			{src: this.NEW_SPRITE_PATH + "Traps/Fan/WindHorizontal_Spritesheet.png", id:"windHorizontal"},      
            {src: this.NEW_SPRITE_PATH + "Traps/Fan/windV.png", id:"windVertical"},
//            {src: this.NEW_SPRITE_PATH + "Traps/Fan/WindVertical_Spritesheet.png", id:"windVertical"},
			//platforms
			{src: this.PLATFORM_PATH + "platform_medium.png", id:"platform_medium_gray"},
			{src: this.PLATFORM_PATH + "platform_medium_dark.png", id:"platform_medium_dark"},
			{src: this.PLATFORM_PATH + "platform_medium_blue.png", id:"platform_medium_blue"},
			{src: this.PLATFORM_PATH + "platform_small.png", id:"platform_small_gray"},
			{src: this.PLATFORM_PATH + "platform_small_dark.png", id:"platform_small_dark"},
			{src: this.PLATFORM_PATH + "platform_small_blue.png", id:"platform_small_blue"},
			
        ];
        
    },
    
    preload: function() {
        this.AssetManager();
        createjs.Sound.initializeDefaultPlugins();
        queue = new createjs.LoadQueue(true);
        queue.installPlugin(createjs.Sound);
//        queue.on("progress", this.assetsProgress, this);
        queue.on("complete", this.assetsComplete, this);  
        createjs.Sound.alernateExtensions = ["mp3"];
        queue.loadManifest(this.manifest);
    },
        
    assetsProgress: function(e) {
        
    },
    
    assetsComplete: function(e) {
         changeGameState(gameStates.MAIN_MENU);
    }
    
}

////creates a game loop
var FPS = 30;
var dt;
function loop() {    
    runGameTimer();
   try {
         currentGameScene.update();

         stage.update();  
    }
   catch(Exception) {
       
   } finally{
 

   }
   

}

//game timer
var gameTimer = 0.0;
var frameCount = 0;
function resetGameTimer() {
    gameTimer = 0;
}

function runGameTimer() {
    frameCount += 1;
    
    if(frameCount % (FPS / 10) === 0) {
        dt = frameCount / FPS;
        gameTimer += dt;   
        frameCount = 0;
    }
}
//
////user input
var KEYCODE_LEFT = 37;
var KEYCODE_UP = 38;
var KEYCODE_RIGHT = 39;
var KEYCODE_DOWN = 40;
var KEYCODE_W = 87;
var KEYCODE_A = 65;
var KEYCODE_S = 83;
var KEYCODE_D = 68;
var KEYCODE_J = 74;
var KEYCODE_P = 80;
var KEYCODE_SPACE = 32;

var lastKeyPressed;
//

//
function handleKeyDown(evt) {

    if(!evt){ var evt = window.event; }  
    switch(evt.keyCode) {
        case KEYCODE_LEFT:  keyStates.VK_ARROW_LEFT  = true;  break;
        case KEYCODE_RIGHT: keyStates.VK_ARROW_RIGHT = true;  break;
        case KEYCODE_DOWN:	keyStates.VK_ARROW_DOWN  = true;  break;           
        case KEYCODE_UP:    keyStates.VK_ARROW_UP    = true;  break;
        
		case KEYCODE_W: {
			keyStates.VK_ARROW_UP  = true;
			lastKeyPressed = 'w';
			break;
		}
		case KEYCODE_A: {
			keyStates.VK_ARROW_LEFT  = true;
			lastKeyPressed = 'a';
			break;
		}       
		case KEYCODE_S: {
			keyStates.VK_ARROW_DOWN  = true;
			lastKeyPressed = 's';
			break;
		}
		case KEYCODE_D: {
			keyStates.VK_ARROW_RIGHT  = true;
			lastKeyPressed = 'd';
			break;
		}
		case KEYCODE_P: {
			break;
		}
        case KEYCODE_SPACE: {
            keyStates.VK_SPACE = true;
			break;
		}
            
    }
}
//
function handleKeyUp(evt) {
    if(!evt){ var evt = window.event; }  
    switch(evt.keyCode) {
        case KEYCODE_LEFT:	keyStates.VK_ARROW_LEFT  = false; player.velocity.x = 0; player.idle(); break;
        case KEYCODE_RIGHT: keyStates.VK_ARROW_RIGHT = false; player.velocity.x = 0; player.idle(); break;
        case KEYCODE_UP:	keyStates.VK_ARROW_UP  = false; player.idle(); break;
        case KEYCODE_DOWN:	keyStates.VK_ARROW_DOWN    = false; player.idle(); break;
        case KEYCODE_J:	    jamieModeOn  = !jamieModeOn; break;
            
		case KEYCODE_W:     keyStates.VK_ARROW_UP  = false; player.idle(); break;
		case KEYCODE_A:     keyStates.VK_ARROW_LEFT = false; player.idle(); break;
		case KEYCODE_S:     keyStates.VK_ARROW_DOWN  = false; player.idle(); break;
		case KEYCODE_D:     keyStates.VK_ARROW_RIGHT    = false; player.idle(); break;
         case KEYCODE_SPACE: {
            keyStates.VK_SPACE = false;
		 	player.idle();	
			break;
		}
    }
}



var mouseX, mouseY;
function mouseInit() {
    stage.on("stagemousemove", function(evt) {
        mouseX = Math.floor(evt.stageX);
        mouseY = Math.floor(evt.stageY);
    });
}
//
////region Primitive Shapescreates primitives
//function drawCircle(x, y, radius, color) {
//    var circle = new createjs.Shape();
//    circle.graphics.beginFill(color).drawCircle(x,y,radius);    
//    return circle;
//}
//
//function getRectangle(x,y, width, height, color) {
//    var rect = new createjs.Shape();
//    rect.graphics.beginFill(color).drawRect(x, y, width, height);
//    return rect;
//}
////endregion Primitive Shapes
//
////region GUI (creates GUI section)
var titleScreenImg;
var gameOverImg;
var levelSignImg;

var btnPlay;
var btnInstruct;
var btnMenu;
var btnContinue;
var btnMute;
var musicMuted = false;
var mainMenuButtons;
var btnCredit;

function makeButtons() {
    
    var buttonSheet = new createjs.SpriteSheet({
       images: ["Assets/img/buttons.png"],
       frames: {width: 93, height: 33, regX: 46, regY: 15},
        animations: {
            playUp: [0, 0, "playUp"],
            playOver: [1, 1, "playOver"],
            playDown: [2, 2, "playDown"],
            instructUp: [3, 3, "instructUp"],
            instructOver: [4, 4, "instructOver"],
            instructDown: [5, 5, "instructDown"],
            menuUp: [6, 6, "menuUp"],
            menuOver: [7, 7, "menuOver"],
            menuDown: [8, 8, "menuDown"],
            continueUp: [9, 9, "continueUp"],
            continueOver: [10, 10, "continueOver"],
            continueDown: [11, 11, "continueDown"],
            creditUp: [12, 12, "creditUp"],
            creditOver: [13, 13, "creditOver"],
            creditDown: [14, 14, "creditDown"],
        } 
    });
    
    
    btnPlay     = new createjs.Sprite(buttonSheet,"playUp");
    bindMenuButtonEvents(btnPlay, "play");

    btnInstruct = new createjs.Sprite(buttonSheet, "instructUp");
    bindMenuButtonEvents(btnInstruct, "instruct");

    btnMenu     = new createjs.Sprite(buttonSheet, "menuUp");
    bindMenuButtonEvents(btnMenu, "menu");

    btnContinue = new createjs.Sprite(buttonSheet, "continueUp");    
    bindMenuButtonEvents(btnContinue, "continue");
    
    btnCredit = new createjs.Sprite(buttonSheet, "creditUp");    
    bindMenuButtonEvents(btnCredit, "credit");
}

function bindMenuButtonEvents(button, name) {
     button.on("mouseover", function(e) {
         this.gotoAndStop(name + "Over");   
     });
    
     button.on("mousedown", function(evt) {
        button.gotoAndStop(name + "Down");   
    });
    
     button.on("mouseout", function(evt) {
        button.gotoAndStop(name + "Up");   
    });
    
 
}

//
////create menu scene
function gameStateMainMenu() {
    var mainMenuScene  = new createjs.Container();
    
    mainMenuScene.MainMenu = function(){
        mainMenuScene.addBg();

      //  mainMenuScene.addTitle();
        mainMenuScene.addButtons();  
    }
    
      mainMenuScene.addBg = function() {
          var shape = new createjs.Shape();
          titleScreenImg = queue.getResult("titleImg");
          shape.scaleX = 800 / titleScreenImg.width;
          shape.scaleY = 600 / titleScreenImg.height;
          shape.graphics.beginBitmapFill(titleScreenImg).drawRect(0,0, 800, 600);
          mainMenuScene.addChild(shape);
      }
    
    mainMenuScene.addButtons = function() {
        mainMenuButtons = new createjs.Container();
        
        btnPlay.x = 200;
        btnPlay.y = 500;
        btnPlay.on('click', function(e) {
             mainMenuButtons.removeAllChildren(btnInstruct,btnPlay,btnCredit);
            changeGameState(gameStates.GAME_SCENE);
        });
        
        btnInstruct.x = 500;
        btnInstruct.y = 500;
        btnInstruct.on('click', function(e) {
            mainMenuButtons.removeChild(btnInstruct,btnPlay,btnCredit);
            
            changeGameState(gameStates.GAME_INSTRUCTION);
        });
        
        btnCredit.x = 350;
        btnCredit.y = 500;
        btnCredit.on('click', function(e) {
            mainMenuButtons.removeChild(btnInstruct,btnPlay,btnCredit);
            
            changeGameState(gameStates.CREDITS);
        });
        
        mainMenuButtons.addChild(btnInstruct, btnPlay,btnCredit);
        mainMenuScene.addChild(mainMenuButtons);
    }
    
    mainMenuScene.playGame = function() {
       changeGameState(gameStates.GAME_SCENE);
    }
    
    mainMenuScene.update = function(){ 
		
    };
    
    mainMenuScene.addTitle = function() {
        var texts = new createjs.Container();
        var title = new createjs.Text("Keep Running", "50px Arial", "#fff");
        title.x = 200;
        title.y = 30;
        
        var credit = new createjs.Text("Credits", "50px Arial", "#0f0");
        credit.x = 250;
        credit.y = 100;
        
        var creditDetail = new createjs.Text("Developer: Yang Yi\n\nDirector: Yang Yi\n\nArtist:Yang Yi\n\nProducer: Yang Yi\n\nLead designer: Yang Yi", "30px Arial", "#0f0");
        creditDetail.x = 200;
        creditDetail.y = 150;
        
        texts.addChild(title,credit, creditDetail);
        mainMenuScene.addChild(texts);
    }
    
    mainMenuScene.MainMenu();
    currentGameScene = mainMenuScene;
    stage.addChild(mainMenuScene);
}
////endregion GUI
//
//
////creates game scene
var bg;
var timeText;
var mousePosText;
var scoreText;
var mainBgm;
var mainCamera;
var healthBar;
var canUpdate = false;
var blocks = [];
var runSprite, punchSprite, slideSprite, blockingSprite;
var playerStates = {
    WALKING:0,    
    HOOKED:1,
    SLIDING:2,
    BLOCKING:3,
    JUMPING:4
};

const jumpHeight = 140;
const slideHeight = 32;

var screenWidth = 800;
var screenHeight = 600;
var screenLeft = 250;
var screenRight = screenWidth - 350; //700
var screenTop = 150;
var screenBottom = screenHeight - 100; //500
var levelContainer = new createjs.Container();

var increment = 3;
//test
var nono = false;
var player =  {
    speed: 20,
    inputPaused: false,
    internalTimer: 0,
    actionTimeLimit: 0,
	velocity: new Vector(0, 0),
 	type: "player",
    godModeOn: false,
    godTimer: 0,
    godTimeLimit: 3.5,
	facingRight: true,
	alive: true,
    
    jumpPaused: false,
    jumpTimeLimit: 2.5,
    jumpTimer: 0.0,
    jumpForce: -40,
    
    invincible: false,
    img: null,
    body: null,
    grappleGaunlet: null,
    isSent: false,
    
    update: function() {
        if(canUpdate)
        {            
			this.body.bodyImg.body.x = 300;
            this.updateInput(); 
            this.updateCollision();
            this.grappleGaunlet.update();
            //make the player flash when the player
            //hits the block
            if(this.godModeOn) {

                if(this.godTimer <= this.godTimeLimit){
                    this.godTimer += dt;
                    this.img.visible = !this.img.visible;
                    
                    if(this.godTimer >= this.godTimeLimit / 2){
                        this.inputPaused = false;
                    }
                    
                } else {
                    this.godTimer = 0;
                    this.godModeOn = false;
                    this.img.visible = true;
                }
            }
            
            if(this.state == playerStates.HOOKED) {
                this.body.bodyImg.body.fixedRotation = false;   
            } else {
                 this.body.bodyImg.body.fixedRotation = true;
            }
            
			
        }
		
    },
    
    updateCollision: function() {

    },  
    
    state:playerStates.WALKING,
 
    updateInput: function() {
        if(this.inputPaused)
        {
            this.internalTimer += dt;

            if(this.internalTimer >= this.actionTimeLimit) {                
                this.internalTimer = this.actionTimeLimit = 0;
            }
        }
        else 
        {
			if(!this.alive && (this.img.currentAnimation == "idle_right" || this.img.currentAnimation == "idle_left")){
					changeGameState(gameStates.GAME_OVER);
			}
			
//			console.log("Player is facing right: " + player.facingRight);
            if(keyStates.VK_ARROW_UP ) {
              if(this.state == playerStates.HOOKED) {
                  this.body.bodyImg.body.SetAwake(true);
                   this.grappleGaunlet.retract();
                }   
            }
            
            if(keyStates.VK_ARROW_DOWN) {
                 if(this.state == playerStates.HOOKED) {
                   this.body.bodyImg.body.SetAwake(true);                    
                   this.grappleGaunlet.release();
                }      
            }
            
            if(keyStates.VK_SPACE) {
                 if(this.state == playerStates.HOOKED) {
                        this.grappleGaunlet.deactivate(playerStates.HOOKED);
                }      
                            
                this.jump();
            }
            
			//horizontal movement
			if(keyStates.VK_ARROW_LEFT || keyStates.VK_ARROW_RIGHT){
				
				if(keyStates.VK_ARROW_LEFT && keyStates.VK_ARROW_RIGHT){
					if(lastKeyPressed == 'a'){ this.moveLeft();}
					else{ this.moveRight(); }
				}
				  if(keyStates.VK_ARROW_LEFT) { 
					this.moveLeft();

				} else if(keyStates.VK_ARROW_RIGHT) { 
					this.moveRight();
				}
			}
			
        }
        
        if(this.jumpPaused) {
            this.jumpTimer += dt;
            
            if(this.jumpTimer >= this.jumpTimeLimit) {
                this.jumpPaused = false;
                this.jumpTimer = 0.0;
            }
        }

    },  
    
    moveLeft: function () {
        var force = this.speed * dt;
        
        if(this.state == playerStates.HOOKED) {
            force *= 0.4;   
        }
        
        this.body.bodyImg.body.ApplyImpulse({x: -force, y: 0}, this.body.bodyImg.body.GetPosition());
		this.facingRight = false;
		this.walk();
    },
    
    moveRight: function() {
        var force = this.speed * dt;
        
        if(this.state == playerStates.HOOKED) {
            force *= 0.4;   
        }
        
        this.body.bodyImg.body.ApplyImpulse({x: force, y: 0}, this.body.bodyImg.body.GetPosition());
		this.facingRight = true;
		this.walk();
    },

    jump: function() {
        if(!this.jumpPaused) {
            this.jumpPaused = true;
            var jumpForceFactor = 1;
                        
            if(this.state == playerStates.HOOKED) {
                jumpForceFactor = 0.7;
            }
            
            this.state = playerStates.JUMPING;
                                                   
            this.body.bodyImg.body.ApplyImpulse({x: 0, y: this.jumpForce * jumpForceFactor}, this.body.bodyImg.body.GetPosition());
        }
    },
    walk: function() {
      this.invincible = false;

        if(this.state != playerStates.HOOKED) { 
            
             this.inputPaused = false;
            if(this.facingRight && (this.img.currentAnimation != "walk_right" && this.img.currentAnimation != "die_right")){
                console.log("starting walk right");
                this.img.gotoAndPlay("walk_right");
            }
            else if(!this.facingRight && (this.img.currentAnimation != "walk_left" && this.img.currentAnimation != "die_left")){
                console.log("starting walk left");
                this.img.gotoAndPlay("walk_left");
            }      
        }
    },
	idle: function(){
		if(this.facingRight && this.img.currentAnimation != "idle_right"){
			console.log("starting idle right");
			this.img.gotoAndPlay("idle_right");
		}
		else if(this.img.currentAnimation != "idle_left"){
			console.log("starting idle left");
			this.img.gotoAndPlay("idle_left");
		}
	},
    die: function(){
		this.alive = false;
		if(this.alive){
			if(this.facingRight && this.img.currentAnimation != "die_right"){
				this.img.gotoAndPlay("die_right");
			}
			else if(this.img.currentAnimation != "die_left"){
				this.img.gotoAndPlay("die_left");
			}
		}
	},
    takeHit: function() {
        if(!this.godModeOn && !jamieModeOn){

        }   
    },
    powerUp: function() {
		//this.img = queue.getResult("powerUpForm");
	},
    shoot: function(mouse) {
        if(this.grappleGaunlet.isAttached) {
            this.grappleGaunlet.deactivate(playerStates.WALKING);
            this.body.bodyImg.body.friction = 0.3;
        }else{
            
            
            var x = this.body.bodyImg.x;
            var y = this.body.bodyImg.y;
            var dir = new box2d.b2Vec2(mouse.stageX - x, mouse.stageY - y);         createjs.Sound.play("shoot", {volume:2});
            this.body.bodyImg.body.friction = 0.01;

            dir.Normalize();
            this.grappleGaunlet.activate(dir);
        }          
    }
}
var count = 0;

function GrappleGaunlet() {
 	this.isActive=false;
	this.rangeUpperBound = 16;
    this.rangeLowerBound = 2;
    this.retractinoSpeed = 0.12;
	this.isAttached= false;
	this.numNodes= 0;
    this.width= 30;
    this.height=30;
    this.speed=30;
    this.hook = null;    
    this.anchorPoint = null;
    this.chainLine = new createjs.Shape();
    
    this.img = new createjs.Bitmap(queue.getResult("grapple"));
    
//    levelContainer.addChild(this.chainLine);
    
	//how will the grapple update? when the player updates
	this.activate = function(dir){

        if(!this.isAttached) {
            this.isActive = true;

            //lerp hook along path
            dir.x = dir.x  * this.speed ;
            dir.y = dir.y  * this.speed ;
            var x = player.body.bodyImg.x;
            var y = player.body.bodyImg.y;
            player.grappleGaunlet.body.bodyImg.body.SetActive( true);

            this.body.bodyImg.body.ApplyImpulse({x:dir.x , y: dir.y}, this.body.bodyImg.body.GetPosition());  
        } 
	};
    
    this.body = new Actor(this.width, this.height, 100,200, box2d.b2Body.b2_dynamicBody, this.img);
    
    this.body.contact = function(contact, impulse, first){

        if(contact.meType == player.type) {
            player.grappleGaunlet.deactivate(playerStates.WALKING);   
        } else if (player.grappleGaunlet.isActive) {
     
            player.grappleGaunlet.createHook(contact);
       }
    };
    
    this.createHook = function(target) {
        if(this.hook == null && this.isHookInValidRange() && this.isActive && !this.isAttached){
            player.grappleGaunlet.isAttached = true;
            player.grappleGaunlet.body.bodyImg.body.SetActive( false);
            player.state = playerStates.HOOKED;  
            
            var playerHookedPos = player.body.bodyImg.body.GetWorldCenter();
            
            this.hook = new box2d.b2Joints.b2DistanceJointDef();
            this.anchorPoint = this.body.bodyImg.body.GetPosition();
            this.hook.Initialize(target.bodyImg.body,
            player.body.bodyImg.body, 
            this.anchorPoint,
             playerHookedPos
            );
            
            this.hook.collideConnected=true;
            this.hook.joint = world.CreateJoint(this.hook);
            player.body.bodyImg.body.SetAwake(true);
            this.hook.joint.m_frequencyHz = 100;
            var lengthOffset = 2;
            this.hook.joint.SetLength(this.getDistanceVectorBetweenPlayerAndGrappleGaunlet().Length() - lengthOffset);
        } else {
            this.deactivate(playerStates.WALKING);
        }
    };
    
    this.deactivate = function(playerState) {
        this.isActive = false;
        this.isAttached = false;
        this.body.bodyImg.body.SetActive( false);

        player.state = playerState;
        if(this.hook != null)
            world.DestroyJoint(this.hook.joint);
        this.hook = null;

    };
    
    this.retract = function() {
        var length = this.hook.joint.GetLength();
        if(length >= this.rangeLowerBound) {                       
            this.hook.joint.SetLength(length - this.retractinoSpeed);  

        }
    };
    
    this.release = function () {
        var length = this.hook.joint.GetLength();
        if(length > this.rangeLowerBound) {
            this.hook.joint.SetLength(length + this.retractinoSpeed);  
      
        }
    };

    this.getDistanceVectorBetweenPlayerAndGrappleGaunlet = function() {
            var playerPos = player.body.bodyImg.body.GetPosition();
            var grapplePos = this.body.bodyImg.body.GetPosition();
            var distanceBetweenPlayerAndGrappleGun = new box2d.b2Vec2(playerPos.x - grapplePos.x, playerPos.y - grapplePos.y);  
            return distanceBetweenPlayerAndGrappleGun;
    };
    
        this.isHookInValidRange = function () {
            var distenceSquared = this.getDistanceVectorBetweenPlayerAndGrappleGaunlet().LengthSquared();
            if(this.rangeUpperBound * this.rangeUpperBound > distenceSquared && 
              this.rangeLowerBound * this.rangeLowerBound < distenceSquared)  {
                return true;   
            }
            return false;
        }
    
	this.update=function() {      
       
        if(!this.isHookInValidRange()){
            this.deactivate(playerStates.WALKING);
            
        }                
        if(!this.isActive && !this.isAttached) {
            var pos = player.body.bodyImg.body.GetPosition();
            var y = pos.y - player.body.bodyImg.body.height + 20 / scale;
            this.body.bodyImg.body.SetPosition(new box2d.b2Vec2(pos.x, y));
            this.body.bodyImg.body.SetAngle(0);
        } 
        
        if(this.isAttached || !this.isActive) {
            this.body.bodyImg.body.SetActive(false);                      
        }
        
         if(this.isAttached || this.isActive) {
                this.chainLine.graphics.clear();   
                this.chainLine.graphics.setStrokeStyle(6);
                this.chainLine.graphics.beginStroke("blue");
                var player_pos = player.body.bodyImg.body.GetPosition();
                var hook_pos = this.body.bodyImg.body.GetPosition();
                var yOffset = player.body.bodyImg.body.height / 2 * scale;
                this.chainLine.graphics.moveTo(player_pos.x * scale, player_pos.y * scale - yOffset);     
                this.chainLine.graphics.lineTo(hook_pos.x * scale, hook_pos.y * scale);
                this.chainLine.graphics.endStroke();   
                
            } else {
                this.chainLine.graphics.clear();   
            }
	}   
}

var blockType = {
    SLIDE_BLOCK:0,
    JUMP_BLOCK:1,
    BREAK_BLOCK:2,
    SPIKE_BLOCK:3,
};

var BlockTypes = [
    blockType.SLIDE_BLOCK,
    blockType.JUMP_BLOCK,
    blockType.BREAK_BLOCK, 
    blockType.SPIKE_BLOCK,

];
    
var testGate;
var testGateLever;
var powerUpTrigger;
var powerUpTimer;
var trapTrigger;
	
	function LeverTrigger(x, y ,width, height, disposable, action, meActor, manifestName, conditional)
{
	this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
	this.that = this;
    this.disposable = disposable;
    this.actor = meActor;
    this.action = action;
    this.condition = conditional; 
    this.active = false;
	this.type = "Trigger";
	
	//representation
    var img = null;
    if(manifestName != null && manifestName != "") {
		var triggerSpritesheet = new createjs.SpriteSheet({
			images: [queue.getResult("trigger")],
			frames: {width:90, height:60, regX:45, regY: 30},
			animations: {
				right:[0, 0],
				left:[14, 14],
				turnRight:[15, 29, "right"],
				turnLeft:[0, 14, "left"],
			}
		});
        img = new createjs.Sprite(triggerSpritesheet, "turnRight");
    }
    
    var imgWidth = img == null ? width : img.getBounds().width;
    var imgHeight = img == null ? height : img.getBounds().height;
    if(img == null)console.log(imgHeight);
    this.body = new meSensor( imgWidth, imgHeight , this.x, this.y, box2d.b2Body.b2_kinematicBody, img);
	this.body.meType = "Trigger";
	
	actors.push(this.body);
	img.x = x;
	img.y = y;
	currentGameScene.addChild(img);
    
    var first = false;

	this.update = function()
    {        
        if(this.condition(this.that))
        {
            this.action(this.actor);  
            this.active = true;
            if(!first)
            {
                 createjs.Sound.play("switchActivate", {volume:2});
                first = true;
            }
        }
		
		//handle animation
		if(this.active){
			if(this.body.bodyImg.currentAnimation != "right" || "turnRight"){
				this.body.bodyImg.gotoAndPlay("turnRight");
			}
		}else{
			if(this.body.bodyImg.currentAnimation != "left" || "goLeft"){
				this.body.bodyImg.gotoAndPlay("goLeft");
			}
		}
    }
    
    this.deactivate = function()
    {
        this.active = false;   
    }
}



function SpikeTrigger(x, y ,width, height, disposable, action, meActor, manifestName, conditional)
{
	this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
	this.that = this;
    this.disposable = disposable;
    this.actor = meActor;
    this.action = action;
    this.condition = conditional; 
    this.active = false;
	this.type = "Trigger";
	
	//representation
	var triggerSpritesheet = new createjs.SpriteSheet({
		images: [queue.getResult("spike")],
		frames: {width:60, height:31, regX:30, regY: 16},
		animations: {
			down:[0, 0, "down"],
			up:[0, 29, "down"],
		}
	});
	img = new createjs.Sprite(triggerSpritesheet, "up");

  	var imgWidth = img == null ? width : img.getBounds().width;
    var imgHeight = img == null ? height : img.getBounds().height;
    if(img == null)console.log(imgHeight);
    this.body = new meSensor( imgWidth, imgHeight , this.x, this.y, box2d.b2Body.b2_kinematicBody, img);
	this.body.meType = "Trigger";
	
	actors.push(this.body);
	img.x = x;
	img.y = y;
	currentGameScene.addChild(img);
    
 
//	img.x = x;
//	img.y = y;

	this.update = function()
    {     
		
        if(this.condition(this.that))
        {
			console.log("condition met");
            this.action(this.actor); 
            this.active = true;
        }
		//handle animation
		if(this.active){
			this.body.bodyImg.gotoAndPlay("up");
			this.active = false;
//			if(this.body.bodyImg.currentAnimation != "up"){
//				this.body.bodyImg.gotoAndPlay("up");
//			}
		}
    }
    
    this.deactivate = function()
    {
        this.active = false;   
    }
}
    
function gameSceneBase(loadScene) {
      var gameScene = new createjs.Container();
    gameScene.GameScene = function() {
        currentGameScene.removeChild();
        currentGameScene = gameScene;
       
        resetGameTimer();
        gameScene.resetGame();
        
        loadScene();
        gameScene.addButtons();
        gameScene.addSound();
        gameScene.addGameObjects();
        gameScene.addGUI();
        //gameScene.addDebugInfo();
        canUpdate = true;
    }
    
    gameScene.resetGame = function() {
        hitCount = 3;
        canUpdate = false;
        runningDistance = 0;
        blocks = [];   
        
        var stageMouseDownNotDefined = stage._listeners.stagemousedown == undefined;

        if(stageMouseDownNotDefined) {
            stage.on("stagemousedown", function(evt){
                if(currentGameState == gameStates.GAME_SCENE) 
                    player.shoot(evt);
            });

        }

    }
    
    gameScene.addButtons = function() {
        var gameButtons = new createjs.Container();
        
        var data = {
             images: ["assets/img/mute.png"],
             frames: {width:32, height:32},
             animations: {unmuted:[0], muted:[1]}
        };
        
        var muteSheet = new createjs.SpriteSheet(data);
        btnMute = new createjs.Sprite(muteSheet, "unmuted");
        btnMute.width =  btnMute.height = 20;
        btnMute.x = 800 - 32;
        btnMute.addEventListener("click", function(e){
            musicMuted = !musicMuted;            
            btnMute.gotoAndStop(musicMuted ? "muted":"unmuted");
            mainBgm.setMute(musicMuted);
        });
    
        gameButtons.addChild(btnMute);
            
        gameScene.addChild(gameButtons);
    }
    
    gameScene.addSound = function() {
        if(!haveIGottenHereYet)
        {
        mainBgm = createjs.Sound.play("mainBGM", {loop: -1, volume:.5});
            haveIGottenHereYet = true;

        if(mainBgm != undefined){
            mainBgm.setMute(false);            

        }
        }
    }

    gameScene.addGUI = function() {
//        var texts = new createjs.Container();
//        scoreText = new createjs.Text("", "30px Arial", "#fff");
//        scoreText.x = 500;
//        scoreText.y = 30
//        
//        var bar = new createjs.Shape();
//		//bar.graphics.beginFill
//        
//        texts.addChild(scoreText);
//        
//        var healthBarBG = getRectangle(10, 100, 100,20, "#F00");
//        healthBar = getRectangle(10, 100, 100, 20, "#0f4");
//        
//        gameScene.addChild(texts,healthBarBG, healthBar);
    }
    
    gameScene.update = function() {
        updateGame();
    }
    
    
     gameScene.addGameObjects = function() {
        //display level sign
//	 	levelSignImg = queue.getResult("levelSign");
//	 	createjs.Tween.get(levelSignImg)
//					 	.wait(500)
//					 	.to({x:100, y:200, rotation:0}, 1500, createjs.Ease.bounceOut)
//					 	.wait(1000)
//					 	.to({y:-160, rotation:0}, 500, createjs.Ease.backIn)
//					 	.call(function(e){});
//	 
//		 
//		 //set up levelContainer
//		levelContainer.removeAllChildren();
//	 	for(var i = 0; i < testPositions.length; i++)
//	 	{
//			var test = drawCircle(testPositions[i].x, testPositions[i].y, 20, "#001");
//		 	levelContainer.addChild(test);
//	 	}
//		
//		 levelContainer.addChild(drawCircle(endNode.x, endNode.y, 20, "#F00"));
		 
		gameScene.addChild(levelContainer);
    }
    
	//timeText, mousePosText, 
    gameScene.addDebugInfo = function() {
//        var t = new createjs.Container();        
//        gameScene.addChild(t);
//
//        timeText = new createjs.Text("Time: " + gameTimer, "20px Arial", "#fff");
//        timeText.x = 10;
//        timeText.y = 50;
//        
//        mousePosText = new createjs.Text("", "20px Arial", "#fff");
//        mousePosText.x = 10;
//        mousePosText.y = 30;
//        
//        t.addChild(timeText, mousePosText);
    }

    gameScene.GameScene();
    stage.addChild(gameScene);
}
    
    
function gameLevelOne() {
    
    console.log("loading level one ");
    gameSceneBase(loadGameSceneOne);
  
}
function gameLevelTwo() {

    console.log("loading level two ");
    gameSceneBase(loadGameSceneTwo);
}
function gameLevelThree() {

    console.log("loading level three ");
    gameSceneBase(loadGameSceneThree);
}
function gameLevelFour() {

    console.log("loading level four ");
    gameSceneBase(loadGameSceneFour);
}
function gameLevelFive() {

    console.log("loading level five ");
    gameSceneBase(loadGameSceneFive);
}
function gameLevelSix() {

    console.log("loading level six ");
    gameSceneBase(loadGameSceneSix);
}


function updateGame(){
    if(canUpdate) {
//      mainCamera.update();
        updatePhysics(dt);
        updateLevelScrolling();
        
        for(i = 0; i < meTriggers.length; i++ ) {
            meTriggers[i].update();   
        }
        
         for(i = 0; i < portalSet.length; i++ ) {
             portalSet[i].update();                          
        }
        
		
        bg.x = (bg.x) % bg.tileW;
        
        runningDistance += 1;
        player.update();

		if(powerUpTimer > 0){
			powerUpTimer -= dt;
		}
		if(player.body.bodyImg.y > 99930){
    		changeGameState(gameStates.GAME_OVER);
		}
		
//		layneTest();
//        timeText.text = "Time: " + gameTimer.toFixed(1);
   //     mousePosText.text = "Mouse pos: " + (mouseX + " " + mouseY);
//        scoreText.text = "distance: " + runningDistance;
    }
}

function updateLevelScrolling() {
    var pos = player.body.bodyImg.body.GetPosition();
    
    var player_pos = {x:pos.x * scale, y:pos.y *scale};

//    player_pos.x = mouseX;
//    player_pos.y = mouseY;
    
    var canvasCenter = {x: canvas.width / 2, y: canvas.height / 2};
    var halfBox = 100;
    
    var restrictionTolerance = 5;
    
    var b2PlayerVel = player.body.bodyImg.body.GetLinearVelocity();
    var vel = {x: Math.abs(b2PlayerVel.x ), y:Math.abs(b2PlayerVel.y)};
    vel.x = vel.x < restrictionTolerance ? restrictionTolerance : vel.x;
    vel.y = vel.y < restrictionTolerance ? restrictionTolerance : vel.y;
    
    var isPlayerAbove = player_pos.y + (-vel.y * scale * dt) <= canvasCenter.y - halfBox;
    var isPlayerLeft = player_pos.x + (-vel.x * scale* dt)  <= canvasCenter.x  - halfBox;
    var isPlayerRight = player_pos.x  + (vel.x * scale* dt) >= canvasCenter.x + halfBox;
    var isPlayerBelow = player_pos.y  + (vel.y  * scale * dt) >= canvasCenter.y + halfBox;
 
   
    for(var i = 0; i < actors.length; i ++){
        var _x = actors[i].bodyImg.body.GetPosition().x;
        var _y = actors[i].bodyImg.body.GetPosition().y;

         if(isPlayerAbove ) {

//            console.log("player above"); 

             _y += vel.y * dt;
         }

         if(isPlayerLeft ) {
            _x += vel.x * dt;
//            console.log("player left");

         }

         if(isPlayerRight ) {
            _x -= vel.x * dt;
//            console.log("player right");
         }

        if(isPlayerBelow ) {

            _y -= vel.y * dt;
//            console.log("player below");

         }

        actors[i].bodyImg.body.SetPosition(new box2d.b2Vec2(_x , _y));
        actors[i].awake = true;
		actors[i].update();
    } 
}



function loadGameSceneBase(playerX, playerY){
    
    init();

//    //load ground


	//load bg
	var bgImg = queue.getResult("bg");
	bg = new createjs.Shape();
	bg.tileW = bgImg.width;
	bg.graphics.beginBitmapFill(bgImg)
	.drawRect(0,0,stage.canvas.width + bgImg.width, stage.canvas.height);

	//load player
	var playerSheet = new createjs.SpriteSheet({
	images: [queue.getResult("player")],
	frames: {width: 150, height: 150, regX: 0, regY: 0},
	animations: {
		 die_left: [0, 47, "idle_left"],
		 die_right: [48, 95, "idle_right"],
		 gethit_left: [96, 143, "gethit_left"],
		 gethit_right: [144, 191, "gethit_right"],
		 idle_left: [192, 239, "idle_left", 1.5],
		 idle_right: [240, 287, "idle_right", 1.5],
		 walk_left: [288, 355, "walk_left", 2],
		 walk_right: [356, 423, "walk_right", 2],
		}     
	});

	player.img =  new createjs.Sprite(playerSheet, "idle_right");  
    
    player.grappleGaunlet = new GrappleGaunlet();


	player.body = new NonRotatingActor(player.img.getBounds().width / 4, player.img.getBounds().height / 2, playerX, playerY, box2d.b2Body.b2_dynamicBody, player.img);
	player.body.meType = "player";
	actors.push(player.body);
    
    actors.push(player.grappleGaunlet.body);    


	currentGameScene.addChild(bg, player.punchCollision, player.blockCollision, player.body.bodyImg);
	currentGameScene.addChild(player.grappleGaunlet.chainLine, player.grappleGaunlet.img);
	//x,y,width,height,finalx,finaly

	//player.img.x = 20;
	//player.img.y = stage.canvas.height;
	console.log("actor num:" + actors.length);
}

function loadGameSceneOne()
{
    loadGameSceneBase(400, 0);   
	//ground
//	addPlatform(0, 0, 2, 600, "platform_small_dark");
//	addPlatform(141, 575, 100, 2, "platform_small_dark");
//
//	//platforms
//	addPlatform(120, 400, 5, 2, "platform_small_dark");
//	addPlatform(500, 300, 5, 2, "platform_small_dark");
//	addPlatform(415, 160, 5, 2, "platform_small_dark");
//	addPlatform(630, 500, 4, 4, "platform_small_dark");
    
    var testGate = new Gate(1000,300,200,500,1000,0);
//	meTriggers.push(new LeverTrigger(700,100,100,100, false, openGate, testGate, "trigger", playerInTrigger));
//	meTriggers.push(new Trigger(1200, 325, 400, 100, false, endLevel, null, "goal", playerInTrigger));
	//        addFan(100, 300, fanDirection.RIGHT);
	//    addPortalPair(100, 100, 600, 400, portalColor.RED);
	//        addPortalPair(100, 300, 750, 500, portalColor.BLUE);

    
    
    addPlatform(140,574,100, 1, "platform_small_dark");
    addPlatform(612,414,5, 1, "platform_small_dark");
    addPlatform(336,271,5, 1, "platform_small_dark");
    meTriggers.push(new LeverTrigger(329,197, 100,100, false, openGate, testGate, "trigger", playerInTrigger));
    meTriggers.push(new Trigger(1244,469,400, 100, false, endLevel, null, "goal", playerInTrigger));
    addPlatform(616,46, 5, 1, "platform_small_dark");
              
    player.body.bodyImg.body.SetPosition(new box2d.b2Vec2(100/scale,400/scale));

	//addPlatform(580, 500, 2, 5, "assets/img/punchDebug.bmp");
}


function loadGameSceneTwo() {
    loadGameSceneBase(400,300);
    addPlatform(140,574, 100 , 2 , "platform_small_dark");  
    addPlatform(715,408, 5 , 1   , "platform_small_dark");
    addPlatform(674,-180, 10 , 1   , "platform_small_dark");
    addPlatform(278,-180, 10 , 1   , "platform_small_dark");
    addPlatform(913,0, 0.5 , 10, "platform_small_dark");   
    
    addPlatform(660,100, 10 , 1 , "platform_small_dark");  
    addPlatform(303,100, 10 , 1 , "platform_small_dark");  
    
    addPlatform(43,217, 0.5 , 20 , "platform_small_dark");  
    addPlatform(43,487, 0.5 , 10 , "platform_small_dark");
    
    addPortalPair(150, 0, 700, 320, portalColor.RED);
    //var testGate = new Gate(1000,300,200,500,1000,0);

	  var gate = new Gate(1040,300,300,200, 1000, 0);
    meTriggers.push(new LeverTrigger(805,55, 100, 100, false, openGate, gate, "trigger", playerInTrigger));
    	meTriggers.push(new Trigger(1500,325, 400, 100, false, endLevel, null, "goal", playerInTrigger));

}  


function loadGameSceneThree() {
    loadGameSceneBase(800,300);
    addPlatform(141,574, 100 , 2   , "platform_small_dark"); 
   
    addPlatform(600,200, 0.5 , 20   , "platform_small_dark"); 
    
    addPlatform(49,351, 0.5 , 20   , "platform_small_dark"); 
    addPlatform(-210,358, 0.5 , 20 , "platform_small_dark");   
   
    addPlatform(29, 15, 10 , 1   , "platform_small_dark"); 
    addPlatform(520,14, 10 , 1   , "platform_small_dark"); 
    addPlatform(813,14, 5 , 1   , "platform_small_dark"); 

    addPlatform(79, 182, 1 , 1  , "platform_small_dark"); 
    addPlatform(123,182, 1 , 1 , "platform_small_dark");  
    addPlatform(156,183, 1 , 1 , "platform_small_dark"); 
        
    
    var gate = new Gate(998,264,0,0, 990,0);

    
	meTriggers.push(new Trigger(130,500,  100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
 meTriggers.push(new Trigger(195,500,  100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
     meTriggers.push(new Trigger(384,500,  100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
     meTriggers.push(new Trigger(563,500,  100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
     meTriggers.push(new Trigger(325,500,  100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
     meTriggers.push(new Trigger(530,500,  100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
	
     meTriggers.push(new LeverTrigger(110,107,  100, 100, false, openGate, gate, "trigger", playerInTrigger)); 
//
//     meTriggers.push(new Trigger(1197,493, 100, 100, false, openGate, gate, "trigger", playerInTrigger));
//    
    

     meTriggers.push(new LeverTrigger(1208,420, 100, 100, false, endLevel, gate, "goal", playerInTrigger));

    addPortalPair(-147,100, 670, 100, portalColor.RED);
    addPortalPair(-12,450,  400, 100, portalColor.BLUE);

}


function loadGameSceneFour() {
    loadGameSceneBase(1350,-650);
    addPlatform(80,574, 100 , 2     , "platform_small_dark"); 
    addPlatform(790,400, 0.5 , 5  , "platform_small_dark"); 
    addPlatform(80,400, 0.5 , 5  , "platform_small_dark"); 

    addPlatform(783,280, 5 , 1 , "platform_small_dark"); 
    addPlatform(317,280, 10 , 1  , "platform_small_dark"); 
    addPlatform(84,163, 0.5 , 5  , "platform_small_dark"); 
    
    addPlatform(335,-448, 0.5 , 20  , "platform_small_dark"); 
    addPlatform(567,-448, 0.5 , 20  , "platform_small_dark"); 
    addPlatform(335,-980, 0.5 , 5  , "platform_small_dark"); 


    addPlatform(220,50, 5 , .5  , "platform_small_dark"); 
    addPlatform(680,50, 5 , .5  , "platform_small_dark"); 

//   

    addPlatform(800,130, 0.5 , 5  , "platform_small_dark"); 

    addPlatform(830,-1100, 20 , 0.5  , "platform_small_dark"); 
     addPlatform(800,-940,10 ,0.5  , "platform_small_dark"); 
     addPlatform(1160,-940,5 ,0.5  , "platform_small_dark"); 
    
    
     addPlatform(1270,-680, .5 ,10  , "platform_small_dark"); 
     addPlatform(1700,-680, .5 ,10  , "platform_small_dark"); 
     addPlatform(1480,-440, 10,0.5  , "platform_small_dark"); 


     addPlatform(1820,-940,5 ,0.5  , "platform_small_dark"); 
     addJumpingBlock(1640,-460, 1 , 1);



    addFan(681,120,fanDirection.LEFT);
    addFan(681,220, fanDirection.LEFT);


        var gate = new Gate(2069,-1218,0,0,1469,-700);

     meTriggers.push(new Trigger(160,220,   100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
     meTriggers.push(new LeverTrigger(1760,-248, 100, 100, false, openGate, gate, "trigger", playerInTrigger)); 
     meTriggers.push(new LeverTrigger(146,487,   100, 100, false, openGate, gate, "trigger", playerInTrigger)); 
     meTriggers.push(new Trigger(2140, -1100,   100, 100, false, endLevel, gate, "goal", playerInTrigger)); 

    

//     meTriggers.push(new Trigger(698,420, 100, 100, false, openGate, gate, "trigger", playerInTrigger));
//
//    addPortalPair(-147,100, 670, 100, portalColor.RED);
//    addPortalPair(-12,450,  400, 100, portalColor.BLUE);   
}


function loadGameSceneFive() {
    loadGameSceneBase(600,100);
    
    addPlatform(141,575, 100 , 2    , "platform_small_dark");
    
    addPlatform(618,0, 10 , 1    , "platform_small_dark");
    addPlatform(204,0, 10 , 1    , "platform_small_dark");
    addPlatform(892,0, 1 , 1    , "platform_small_dark");

    addPlatform(-31,283, 0.5 , 20    , "platform_small_dark");
    addPlatform(-32,472, 0.5 , 10    , "platform_small_dark");
  
    addPlatform(370,300, 10 , 1    , "platform_small_dark");
    addPlatform(743,300, 10 , 1    , "platform_small_dark");
    

    addPlatform(1000,520, 0.5 , 10    , "platform_small_dark");
    addPlatform(1500,520, 0.5 , 10   , "platform_small_dark"); 
    addPlatform(1260,300, 10 , 1    , "platform_small_dark");

    var gate = new Gate(1032, -38, 0, 0, 1000, -200);
    addFan(48,521, fanDirection.UP);
    addFan(0,442,fanDirection.RIGHT);

    meTriggers.push(new LeverTrigger(1100,480, 100, 100, false, openGate, gate, "trigger", playerInTrigger)); 
    meTriggers.push(new Trigger(35,70,100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
    meTriggers.push(new Trigger(1200, 100,100, 100, false, endLevel, gate, "goal", playerInTrigger));
    
    addPortalPair(930, 450, 1420, 450, portalColor.BLUE);
}


function loadGameSceneSix() {
    loadGameSceneBase(-1050,0);
    
    addPlatform(141,575, 100 , 2    , "platform_small_dark");
    
    addPlatform(141,575, 100 , 2  , "platform_medium_dark"); 
    addPlatform(682,-352, 10 , 1  , "platform_small_dark"); 
    addPlatform(220,-352, 10 , 1  , "platform_small_dark"); 
    addPlatform(-248,-353, 10 , 1 , "platform_small_dark");  
    addPlatform(639,487, 10 , 1   , "platform_small_dark");

    addPlatform(-852,481, 5 , 1  , "platform_small_dark"); 
    
    addPlatform(-1047,286, 5 , 1 , "platform_small_dark");  
    addPlatform(-1032,-114, 5 , 1 , "platform_small_dark");  
    addPlatform(-792,-114, 5 , 1  , "platform_small_dark"); 

    addPlatform(1024,-126, 5 , 1   , "platform_small_dark");
    addPlatform(1024,-346, 5 , 1   , "platform_small_dark");

    addPlatform(-749,-352, 10 , 1   , "platform_medium_gray");
    
    addPlatform(-1048,480, 5 , 1   , "platform_small_dark");
    addPlatform(-1156,187, 0.5 , 10 , "platform_small_dark");  
    addPlatform(-1155,-188, 0.5 , 10, "platform_small_dark");   
    
    addPlatform(1156,27, 0.5 , 10 , "platform_small_dark");  
    addPlatform(1155,-488, 0.5 , 10, "platform_small_dark"); 
    
  addPlatform(1048,480, 5 , 1   , "platform_small_dark");

    
    addPlatform(221,-5, 5 , 1   , "platform_small_dark");
    addPlatform(229,163, 1 , 1 , "platform_small_dark");
	
    addJumpingBlock(-147, 489, 1 , .5);
    addFan(-956,379, fanDirection.RIGHT);   
    addPortalPair(1024, -200, -748, - 200, portalColor.BLUE);
	

    ////spikes
    var gate = new Gate(1290,200, 1400, 100);     

    meTriggers.push(new  Trigger(-259,500, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
    meTriggers.push(new  Trigger(-347,500, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
    meTriggers.push(new  Trigger(-424,500, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
    meTriggers.push(new  Trigger(-515,500, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
    meTriggers.push(new  Trigger(-602,500, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
    meTriggers.push(new  Trigger(-690,500, 100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
    ////spikes
     meTriggers.push(new Trigger(346,517, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
     meTriggers.push(new Trigger(247,516, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
     meTriggers.push(new Trigger(154,518, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));  
     meTriggers.push(new Trigger(58,513,  100, 100, false, trapDeath, gate, "trap", playerInTrigger)); 
     meTriggers.push(new Trigger(-37,518, 100, 100, false, trapDeath, gate, "trap", playerInTrigger));                
     meTriggers.push(new LeverTrigger(-1000,-200, 100, 100, false, openGate, gate, "trigger", playerInTrigger));                 
     meTriggers.push(new Trigger(1390, 500, 100, 100, false, endLevel, gate, "goal", playerInTrigger));                 
    
//    addPortalPair(930, 450, 1420, 450, portalColor.BLUE);
}

function doNothing() {}

function powerUp(power){
	console.log("powerUp");
	player.speed = 20;
	player.powerUp();
	//player.img = queue.getResult("powerUpForm");
}


var endGameTrigger; 
           
function trapDeath(blah){
	if(player.body.bodyImg.currentAnimation != "die_right" || player.body.bodyImg.currentAnimation != "die_left" )
//	console.log("trap death: " + blah);
	//blah.active = true;
	player.die();
}


function endLevel(nixt)
{
	console.log("end");
    currentLevel++;
    changeGameState(gameStates.LEVEL_COMPLETE);
}



function addPlatform(_x, _y, _scaleX, _scaleY, path){
 	var platformImage = new createjs.Bitmap(queue.getResult(path));
	console.log(path);
	platformImage.scaleX *= _scaleX;
    platformImage.scaleY *= _scaleY;

	var width = platformImage.getBounds().width;
	console.log("mid: " + width);
	if(width == null){
		console.log("errorous width");
		width = platformImage.width;
	}
    var platform = new Actor(platformImage.getBounds().width * platformImage.scaleX, platformImage.getBounds().height * platformImage.scaleY, _x, _y, box2d.b2Body.b2_staticBody, platformImage);
	//platform.blockType = blockType.SPIKE_BLOCK;
   
	//add platform actor
	actors.push(platform);
	//add image to container
	levelContainer.addChild(platform.bodyImg);
}

function JumpingBlock(_x, _y) {     
	var jumpblockSpriteSheet = new createjs.SpriteSheet({
		images: [queue.getResult("jumpblock2")],
		frames: {width: 120, height: 50, regX: 0, regY: 0},
		animations: {
			 neutral: [0, 0],
			 bounce: [0, 14, "neutral"],
			}
		});
	var jumpblockSprite = new createjs.Sprite(jumpblockSpriteSheet, "neutral");
	jumpblockSprite.x = _x;
	jumpblockSprite.y = _y;

	var w = jumpblockSprite.getBounds().width;
	var h = jumpblockSprite.getBounds().height;

	this.actor = new Actor(w, h, _x, _y, box2d.b2Body.b2_staticBody, jumpblockSprite);
	this.actor.contact = function(){
		player.body.bodyImg.body.ApplyImpulse({x: 0, y: player.jumpForce}, player.body.bodyImg.body.GetPosition());  
		this.bodyImg.gotoAndPlay("bounce");
	};
}

function addJumpingBlock(_x, _y, _scaleX, _scaleY) {
	var jumpingBlock = new JumpingBlock(_x, _y);
//	jumpingBlock.createNew(_x, _y);
	
	actors.push(jumpingBlock.actor);
	jumpingBlock.actor.bodyImg.x = _x;
	jumpingBlock.actor.bodyImg.y = _y;
	levelContainer.addChild(jumpingBlock.actor.bodyImg);
}

                                
function playerInTrigger(rect)
{
	var left = rect.body.bodyImg.x - rect.body.bodyImg.getBounds().width/2;
	var right = rect.body.bodyImg.x + rect.body.bodyImg.getBounds().width/2;
	var top = rect.body.bodyImg.y - rect.body.bodyImg.getBounds().height/2;
	var bottom = rect.body.bodyImg.y + rect.body.bodyImg.getBounds().height/2;
	
	if(player.body.bodyImg.x > left && player.body.bodyImg.x < right
	  && player.body.bodyImg.y > top && player.body.bodyImg.y < bottom){
		
		return true;
	}
	return false;
}

function gameStateCredits() {
    var creditsScene = new createjs.Container();
    
    creditsScene.CreditsScene = function() {   
        creditsScene.addBg();
        creditsScene.addButtons();
    }
    
   	creditsScene.addBg = function() {        
		var shape = new createjs.Shape();
		var creditImg = queue.getResult("credits");
//		shape.scaleX = 800 / creditImg.width;
//		shape.scaleY = 600 / creditImg.height;
		shape.graphics.beginBitmapFill(creditImg).drawRect(0,0, 800, 600);
		creditsScene.addChild(shape);
   }
			
		creditsScene.addButtons = function() {
        var gameOverButtons = new createjs.Container();
        
        btnMenu.x = 250;
        btnMenu.y = 500;        
                
        btnMenu.on('click', function(e) {           
             changeGameState(gameStates.MAIN_MENU);
             mainMenuButtons.removeChild(btnContinue,btnMenu);
        });
                
        gameOverButtons.addChild(btnMenu);
        creditsScene.addChild(gameOverButtons);
    }
   
    creditsScene.update = function (){};
    
    currentGameScene.removeAllChildren();
    
    currentGameScene = creditsScene;
 
    creditsScene.CreditsScene();
    
    stage.addChild(creditsScene);
    
}






function gameStateLevelComplete(){
	var gameOverScene = new createjs.Container();
    
	gameOverScene.GameOverScene = function() {   
        gameOverScene.addBg();
        gameOverScene.addButtons();
        gameOverScene.addGUI();
    }
    
   	gameOverScene.addBg = function() {        
		var shape = new createjs.Shape();
		var gameoverImg = queue.getResult("levelComplete");
		shape.scaleX = 800 / gameoverImg.width;
		shape.scaleY = 600 / gameoverImg.height;
		shape.graphics.beginBitmapFill(gameoverImg).drawRect(0,0, 800, 600);
		gameOverScene.addChild(shape);
   }
			
		gameOverScene.addButtons = function() {
        var gameOverButtons = new createjs.Container();
        gameOverButtons.addChild(btnContinue, btnMenu);
        
        btnContinue.x = 250;
        btnContinue.y = 500;
        btnContinue.on('click', function(e) {
           
             loadLevel(currentLevel);

             mainMenuButtons.removeChild(btnContinue,btnMenu);
        });
        
        
        btnMenu.x = 600;
        btnMenu.y = 500;
        btnMenu.on('click', function(e) {
                    
            changeGameState(gameStates.MAIN_MENU);
               mainMenuButtons.removeChild(btnContinue,btnMenu); 
        });
        
        gameOverScene.addChild(gameOverButtons);
    }

    gameOverScene.addGUI = function() {
        var texts = new createjs.Container();
        gameOverScene.addChild(texts);   
    }
    
    gameOverScene.update = function (){};
    
    currentGameScene.removeAllChildren();
    
    currentGameScene = gameOverScene;
 
    gameOverScene.GameOverScene();
    
    stage.addChild(gameOverScene);
}


//creates game over scene
function gameStateGameOver() {
	var gameOverScene = new createjs.Container();
	gameOverScene.GameOverScene = function() {   
        gameOverScene.addBg();
        gameOverScene.addButtons();
        gameOverScene.addGUI();
    }
    
   	gameOverScene.addBg = function() {        
		var shape = new createjs.Shape();
		var gameoverImg = queue.getResult("gameOverImg");
		shape.scaleX = 800 / gameoverImg.width;
		shape.scaleY = 600 / gameoverImg.height;
		shape.graphics.beginBitmapFill(gameoverImg).drawRect(0,0, 800, 600);
		gameOverScene.addChild(shape);
   }
    
    gameOverScene.addButtons = function() {
        var gameOverButtons = new createjs.Container();

        gameOverButtons.addChild(btnContinue, btnMenu);
        
        btnContinue.x = 250;
        btnContinue.y = 500;
        
        btnContinue.on('click', function(e) {
             loadLevel(currentLevel);
             mainMenuButtons.removeChild(btnContinue,btnMenu);
        });
        
        btnMenu.x = 600;
        btnMenu.y = 500;
        btnMenu.on('click', function(e) {
                    
            changeGameState(gameStates.MAIN_MENU);
               mainMenuButtons.removeChild(btnContinue,btnMenu); 
        });
        
        gameOverScene.addChild(gameOverButtons);
    }

    gameOverScene.addGUI = function() {
        var texts = new createjs.Container();
//        scoreText = new createjs.Text("You have run: " + runningDistance + "m", "30px Arial", "#0F7");
//        scoreText.x = 250;
//        scoreText.y = 50
        
//        texts.addChild(scoreText);
        gameOverScene.addChild(texts);   
    }
    
    gameOverScene.update = function (){};
    
    currentGameScene = gameOverScene;
 
    gameOverScene.GameOverScene();
    
    stage.addChild(gameOverScene);

}

//creates game Instructions scene
function gameStateInstruction() {
   var gameInstruction = new createjs.Container();
     
    gameInstruction.GameInstruction = function() {  
        gameInstruction.addBg();
        gameInstruction.addButtons();
//        gameInstruction.addGUI();
    }
    
    gameInstruction.addBg = function() {
            
        var shape = new createjs.Shape();
            var instructionImg = queue.getResult("instructionImg");
          shape.scaleX = 800 / instructionImg.width;
          shape.scaleY = 600 / instructionImg.height;
            shape.graphics.beginBitmapFill(instructionImg).drawRect(0,0, 800, 600);
            gameInstruction.addChild(shape);
       }
    
    gameInstruction.addButtons = function() {
        var gameInstructionButtons = new createjs.Container();
        gameInstructionButtons.addChild(btnMenu);
        
        btnMenu.x = 400;
        btnMenu.y = 500;
        btnMenu.on('click', function(e) {
            changeGameState(gameStates.MAIN_MENU);
        });
        
        gameInstruction.addChild(gameInstructionButtons);
    }

    gameInstruction.addGUI = function() {
        var texts = new createjs.Container();
        var instructionText = new createjs.Text("this is a parkour game\n\n Arrow Up: jump(jumps over short blocks)\n\n Arrow Down: slide (slides over mid blocks)\n\n Arrow Left: block (protects you from the spike blocks)\n\n Arrow Right: punch (breaks cracked blocks)", "30px Arial", "#fff");
        instructionText.x = 100;
        instructionText.y = 30
        
        texts.addChild(instructionText);
        gameInstruction.addChild(texts);   
    }
    
    gameInstruction.update = function (){};
    
    gameInstruction.GameInstruction();

    currentGameScene = gameInstruction;
 
    stage.addChild(currentGameScene);

}
//
//
////creates canvas
var stage;
var canvas;
//var debug;
function setupCanvas() {
    
    canvas = document.getElementById("game");
    canvas.width = 800;
    canvas.height = 600;
    canvas.style.marginleft = "auto";
    canvas.style.backgroundColor = "#02A";
    
//    debug = document.getElementById("debug");
//    debug.width = 800;
//    debug.height = 600;
    
    
    stage = new createjs.Stage(canvas);
    stage.enableMouseOver();
    mouseInit();
    createjs.Ticker.addEventListener("tick", loop);
    createjs.Ticker.setFPS(FPS);//    console.log("canvas initialized");
    
    AssetManager.preload();
//    init();
    console.log("assets loaded");
}
//
function main() {
    setupCanvas();
    makeButtons();
    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp; 
}

if (!!(window.addEventListener)) {
    window.addEventListener("DOMContentLoaded", main);
} else {
    window.attachEvent("onload", main);   
}

var meGate;
var meTrigger;

function Gate(x, y, width, height, openX, openY)
{  
   	this.shape = new createjs.Shape();
    this.x = x;
    this.y = y;
    this.changeX = openX - x;
    this.changeY = openY - y;
    this.startingX = x;
    this.startingY = y;
    
    this.width = width;
    this.height = height;
    
   this.shape.graphics.beginFill("#FC5").drawRect(0,0,this.width,this.height);
    
    this.shape.x = x;
    this.shape.y = y;
    
    var img = new createjs.Bitmap(queue.getResult("gate"));
    var sx =  this.width / img.getBounds().width ;
    var sy =  this.height / img.getBounds().height ;
    console.log(this.height + ","+img.getBounds().height );

//    img.scaleY = sy * 0.7;
//    img.regY =  img.getBounds().height ;
//    img.regX = img.getBounds().width / 2;
//    img.regX += width /sx / 2 ;

//    levelContainer.addChild(img);
    currentGameScene.addChild(img);
    console.log("img" + img.getBounds().height);
    this.body = new Actor( img.getBounds().width , img.getBounds().height , this.x, this.y, box2d.b2Body.b2_kinematicBody, img);
//    (player.img.getBounds().width / 2, player.img.getBounds().height, 400, 0, box2d.b2Body.b2_dynamicBody, player.img);
//    levelContainer.addChild(this.body);

    actors.push(this.body);
//    
    this.open = false;

    this.update = function()
    {
       //this.shape.x = this.x;
       //this.shape.y = this.y;
       //this.body.bodyImg.body.SetPosition(new box2d.b2Vec2(this.x / scale, this.y / scale));
		
       //console.log(this.body.bodyImg.x +"," + this.body.bodyImg.y);
    }
}


var fanDirection = {
    UP:0,    
    LEFT:1,
    RIGHT:2
};  

var FANFORCE = 30;
function getFanForceByDirection(dir) {
    var fanForces = [
    new box2d.b2Vec2(0, -FANFORCE), 
                new box2d.b2Vec2(-FANFORCE, 0),
                new box2d.b2Vec2(FANFORCE, 0)];
    
    return fanForces[dir];
}

function getWindImgByDirection(dir) {
    
    if(dir == fanDirection.UP) {
        return new createjs.Bitmap(queue.getResult("windUp"));
    }
    if(dir == fanDirection.LEFT || dir == fanDirection.RIGHT) {
        return new createjs.Bitmap(queue.getResult("windHorizontal"));
    }
}

function getWindTriggerByDirection(dir, _x, _y, w, h, callback, body) {
    var trigger = null;
    if(dir == fanDirection.UP) {
        trigger= new Trigger(_x, _y - h - 100, w, h, false, callback, null, "windVertical", playerInTrigger);
        
    } else if(dir == fanDirection.LEFT) {
        trigger = new Trigger(_x - w - 60, _y, w, h, false, callback, null, "windHorizontal", playerInTrigger);
        body.SetAngle( -90 / 180 * Math.PI);
        
    } else if(dir == fanDirection.RIGHT) {
        trigger = new Trigger(_x + w + 60, _y, w, h, false, callback, null, "windHorizontal", playerInTrigger);
        body.SetAngle( 90 / 180 * Math.PI);
    }
    
    return trigger;
}

    
function Fan(_x, _y, direction) {        
    var force = getFanForceByDirection(direction);
    
    var fanSheet = new createjs.SpriteSheet({
	images: [queue.getResult("fan")],
	frames: {width: 100, height: 50, regX: 0, regY: 0},
	animations: {
		 forward: [0, 13],
		 backword: [14, 27],
		}     
	});
    
    var fanImg = new createjs.Sprite(fanSheet, "forward");
	fanImg.x = _x;
	fanImg.y = _y;
	
    if(direction == fanDirection.LEFT ) {
           fanImg.rotation = -90;
    } else if(direction == fanDirection.RIGHT) {
           fanImg.rotation = 90;
    }
    
    var w = fanImg.getBounds().width;
    var h = fanImg.getBounds().height;

    this.actor = new Actor(w, h, _x, _y, box2d.b2Body.b2_staticBody, fanImg);
	
	//levelContainer.addChild(fanImg);
    this.contactPlayer = function () {
//        console.log(force);
        player.body.bodyImg.body.ApplyForce({x: force.x, y: force.y}, player.body.bodyImg.body.GetPosition());
    }  
    
     this.sensor = getWindTriggerByDirection(direction, _x, _y, w, h, this.contactPlayer, this.actor.bodyImg.body);
}

function addFan(_x, _y, direction) {
    var fan = new Fan(_x, _y, direction);
       
	//add platform actor
	actors.push(fan.actor);
    meTriggers.push(fan.sensor);
	
	fan.actor.bodyImg.x = _x;
	fan.actor.bodyImg.y = _y;
	currentGameScene.addChild(fan.actor.bodyImg);
}

function JumpingBlock(_x, _y) {     
	var jumpblockSpriteSheet = new createjs.SpriteSheet({
		images: [queue.getResult("jumpblock2")],
		frames: {width: 120, height: 50, regX: 0, regY: 0},
		animations: {
			 neutral: [0, 0],
			 bounce: [0, 14, "neutral"],
			}
		});
	var jumpblockSprite = new createjs.Sprite(jumpblockSpriteSheet, "neutral");
	jumpblockSprite.x = _x;
	jumpblockSprite.y = _y;

	var w = jumpblockSprite.getBounds().width;
	var h = jumpblockSprite.getBounds().height;

	this.actor = new Actor(w, h, _x, _y, box2d.b2Body.b2_staticBody, jumpblockSprite);
	this.actor.contact = function(){
		player.body.bodyImg.body.ApplyImpulse({x: 0, y: player.jumpForce * 2}, player.body.bodyImg.body.GetPosition());  
		this.bodyImg.gotoAndPlay("bounce");
	};
}



function addJumpingBlock(_x, _y, _scaleX, _scaleY) {
	var jumpingBlock = new JumpingBlock(_x, _y);
//	jumpingBlock.createNew(_x, _y);
	
	actors.push(jumpingBlock.actor);
	jumpingBlock.actor.bodyImg.x = _x;
	jumpingBlock.actor.bodyImg.y = _y;
	levelContainer.addChild(jumpingBlock.actor.bodyImg);
}


var portalSet = [];    
    
var portalColor = {
    RED: 0,
    YELLOW: 1,
    BLUE: 2
};
 
var portalColorArray = [
    "portalRed",
    "portalYellow",
    "portalBlue",
];
    
function getPortalImageByColor(portalColor) {
    return new createjs.Bitmap(queue.getResult(portalColorArray[portalColor]));
}
    
function Portal(color, x, y) {

    this.color = color;
    this.hasPlayer = false;
    this.destination = null;
     var that = this; 
    this.send = function ( ) {
        if(!player.isSent) {
			player.grappleGaunlet.deactivate(player.state);
			player.body.bodyImg.body.SetPosition(that.destination.sensor.body.bodyImg.body.GetPosition());
            player.body.update();
            that.destination.hasPlayer = true;
            that.hasPlayer = false;
            player.isSent = true; 
        }
    };
    
    this.update = function() {
        if(that.hasPlayer && !playerInTrigger(that.sensor)) {               
            player.isSent = false;
            that.hasPlayer = false;
        }
    }
    
//    console.log( portalColor]);
    this.sensor = new Trigger(x, y, 128, 128,false, this.send, null,  portalColorArray[color], playerInTrigger);  
}
  
function addPortalPair(x1, y1, x2, y2, color) {
    var p1 = new Portal(color, x1, y1);
    var p2 = new Portal(color, x2, y2);
    p1.id = 1;
    p2.id  =2;
    p1.destination = p2;
    p2.destination = p1;
    
    meTriggers.push(p1.sensor);
    meTriggers.push(p2.sensor);

    portalSet.push(p1);
    portalSet.push(p2);
}

function toggleGate(gate)
{
    gate.open = !gate.open;
}


function openGate(gate)
{
	console.log("gate");
	if(!gate.open){
		//gate.x += gate.changeX;
		//gate.y += gate.changeY;
		gate.open = true;
		console.log("before");
		var _x = gate.body.bodyImg.body.GetPosition().x + gate.changeX;
		var _y = gate.body.bodyImg.body.GetPosition().y + gate.changeY;
		console.log("after");
		gate.body.bodyImg.body.SetPosition(new box2d.b2Vec2(_x, _y));
	}
}



function closeGate(gate)
{
	if(gate.open){
		gate.open = false;   
		
		var _x = gate.body.bodyImg.body.GetPosition().x - gate.changeX;
		var _y = gate.body.bodyImg.body.GetPosition().y - gate.changeY;
		gate.body.bodyImg.body.SetPosition(new box2d.b2Vec2(_x, _y));
	}
}

function addGate(x,y,width,height,openX, openY)
{
    meGate = new Gate(x,y,width,height,openX, openY);
}

function addTrigger(x,y,width,height,disposable,action, meActor, conditional)
{
    meTrigger = new Trigger(x,y,width,height,disposable,action, meActor, conditional);
}

function Trigger(x, y ,width, height, disposable, action, meActor, manifestName, conditional)
{
    
    this.width = width;
    this.height = height;
	this.that = this;
    this.disposable = disposable;
	this.x = x;
    this.y = y;
    this.actor = meActor;
    this.action = action;
    this.condition = conditional; 
    this.active = false;
	this.type = "Trigger";
	
	//representation
    
    var img = null;
//	console.log("img: " + img);
    if(manifestName != null && manifestName != "") {
        img = new createjs.Bitmap(queue.getResult(manifestName));
        currentGameScene.addChild(img);
    }
    
    var imgWidth = img == null ? width : img.getBounds().width;
    var imgHeight = img == null ? height : img.getBounds().height;
    if(img == null)console.log(imgHeight);
    this.body = new meSensor( imgWidth, imgHeight , this.x, this.y, box2d.b2Body.b2_kinematicBody, img);
    
	this.body.meType = "Trigger";
	actors.push(this.body);
	
    this.update = function()
    {        
        if(this.condition(this.that))
        {
            this.action(this.actor);  
            this.active = true;
        }
//        console.log("Trigger update");
    }
    
    this.deactivate = function()
    {
        this.active = false;   
    }
}


function addTimedTrigger(x,y,width,height,action, reaction,  meActor, conditional, duration)
{
    meTrigger = new TimedTrigger(x,y,width,height, action, reaction,  meActor, conditional, duration );
}

function TimedTrigger(x, y ,width, height, action, reaction, meActor, conditional, duration)
{
        
    this.shape = new createjs.Shape();
    
    this.x = x;
    this.y = y;
    this.that = this;
    this.width = width;
    this.height = height;
    this.reaction = reaction;
    this.actor = meActor;
    this.action = action;
    this.condition = conditional; 
    this.active = false;
    this.timer = new TickDownTimer(duration);
    
        
   this.shape.graphics.beginFill("#0FB").drawRect(0,0,this.width,this.height);
    
    this.shape.x = x;
    this.shape.y = y;
    levelContainer.addChild(this.shape);
    
    this.update = function()
    {        
        this.shape.x = this.x;
        this.shape.y = this.y;
        if(!this.active)
        {
            if(this.condition(this.that))
            {
                this.action(this.actor);  
                this.active = true;
                this.timer.start();
            }
        }
        else{
         this.timer.update();
         this.active = this.timer.active;
         if(!this.active)
         {
          this.reaction(this.actor);
            this.timer.resetTimer()
         }
        }
        
    }
}

function testConditional(trigger)
{
 return mouseX > trigger.x && mouseX < trigger.x + trigger.width   
}

function ColterTest()
{
    console.log("Got it");
}


var colterTimer;
function TickDownTimer(duration)
{
    this.active = false;
    this.timeLeft = duration;
    this.initialTime = this.timeLeft;
    this.frameCount = 0;
    this.dt = 0.0;

    this.start = function()
    {
        this.active = true;
    }
    
    this.update = function()
    {
        if(this.active)
        {
            this.frameCount += 1;
            if(this.frameCount % (FPS / 10) === 0) {
                 this.dt = this.frameCount / FPS;
                this.timeLeft -= this.dt;   
                this.frameCount=0;
            }
            this.active =  this.timeLeft>0;
        }
//                    console.log("Is timer active? : " + this.active);
        
    }
    
    this.resetTimer = function()
    {
        this.timeLeft = this.initialTime;
        this.active = false;
    }

}




var box2d = {
	b2Vec2 : Box2D.Common.Math.b2Vec2,
	b2BodyDef : Box2D.Dynamics.b2BodyDef,
	b2Body : Box2D.Dynamics.b2Body,
	b2FixtureDef : Box2D.Dynamics.b2FixtureDef,
	b2Fixture : Box2D.Dynamics.b2Fixture,
	b2World : Box2D.Dynamics.b2World,
	b2MassData : Box2D.Collision.Shapes.b2MassData,
	b2PolygonShape : Box2D.Collision.Shapes.b2PolygonShape,
	b2CircleShape : Box2D.Collision.Shapes.b2CircleShape,
	b2DebugDraw : Box2D.Dynamics.b2DebugDraw,
    b2Joints: Box2D.Dynamics.Joints,
    b2ContactListener : Box2D.Dynamics.b2ContactListener,
};
var scale = 30;

var world;
var context;
var actors =[];


function init() {
    world = new box2d.b2World(new box2d.b2Vec2(0, 9.80),true);      
    
    var canvas = document.getElementById("game");
    canvas.width = 800;
    canvas.height = 600;
    
    var debugDraw = new box2d.b2DebugDraw();
    context = debug.getContext('2d');
    
//    debugDraw.SetSprite(context);
//    debugDraw.SetDrawScale(scale);
//    debugDraw.SetFillAlpha(0.5);
//    debugDraw.SetFlags(box2d.b2DebugDraw.e_shapeBit | box2d.b2DebugDraw.e_jointBit);
//    world.SetDebugDraw(debugDraw);
//    
    var listener = new box2d.b2ContactListener();
//    console.log(listener);
    listener.PostSolve = function(contact, impulse) {
		var bodyA = contact.GetFixtureA().GetBody().GetUserData();
		var bodyB = contact.GetFixtureB().GetBody().GetUserData();
        
        
//        console.log(bodyB.contact);
//        console.log(bodyA.contact);

		//true = hit someone, false = someone hit you
        if (bodyA.contact) {

            bodyA.contact(bodyB, impulse, true)
        }
        if (bodyB.contact) {
            bodyB.contact(bodyA, impulse, false)
        }
    }
    
    world.SetContactListener(listener);
   
}

 function Actor(width,height,pX,pY,type, img){
        this.bodyImg = img;
        this.bodyImg.regY += img.getBounds().height / 2 ;
        this.bodyImg.regX += img.getBounds().width / 2;
        
        this.fixDef = new Box2D.Dynamics.b2FixtureDef;
        this.fixDef.density = 1.0;
        this.fixDef.friction = 0.5;
        this.fixDef.restitution = 0.2;
      
        var bodyDef = new box2d.b2BodyDef;
        bodyDef.type = type;

	 	bodyDef.position.x = pX / scale;
        bodyDef.position.y = pY / scale;
        this.fixDef.shape = new box2d.b2PolygonShape();
     
       this.fixDef.shape.SetAsBox((width / scale) / 2, height / scale / 2);
       this.bodyImg.body = world.CreateBody(bodyDef);
        this.bodyImg.body.SetUserData(this);
       this.bodyImg.body.CreateFixture(this.fixDef);
         this.bodyImg.body.width = width / scale;
          this.bodyImg.body.height = height / scale;

      this.update = function () {
            this.bodyImg.x = this.bodyImg.body.GetPosition().x * scale;
            this.bodyImg.y = this.bodyImg.body.GetPosition().y * scale;
            this.bodyImg.rotation = this.bodyImg.body.GetAngle() * (180 / Math.PI);
      }
 }

      
      
function meSensor(width,height,pX,pY,type, img)
{
    if(img != null) {
        this.bodyImg = img;
        this.bodyImg.regY += img.getBounds().height /2 ;
        this.bodyImg.regX += img.getBounds().width / 2;
    	this.type = "trigger";
    } else {
        this.bodyImg = new Object();
    }
        
        var fixDef = new Box2D.Dynamics.b2FixtureDef;
        fixDef.density = 1.0;
        fixDef.friction = 0.5;
        fixDef.restitution = 0.2;
      
        var bodyDef = new box2d.b2BodyDef;
        bodyDef.type = type;

	 	bodyDef.position.x = pX / scale;
        bodyDef.position.y = pY / scale;
        fixDef.shape = new box2d.b2PolygonShape();
     
       	fixDef.shape.SetAsBox((width / scale) / 2, (height / scale) / 2);
       	this.bodyImg.body = world.CreateBody(bodyDef);
        this.bodyImg.body.SetUserData(this);
		fixDef.isSensor=true;
      this.bodyImg.body.width = width / scale;
          this.bodyImg.body.height = height / scale;

       	this.bodyImg.body.CreateFixture(fixDef);

        
		  this.update = function () {
               if(this.bodyImg != null) {
				this.bodyImg.x = this.bodyImg.body.GetPosition().x * scale;
				this.bodyImg.y = this.bodyImg.body.GetPosition().y * scale;
				this.bodyImg.rotation = this.bodyImg.body.GetAngle() * (180 / Math.PI);
               }
		  }
}

 function NonRotatingActor(width,height,pX,pY,type, img){
       this.bodyImg = img;
        this.bodyImg.regY += height + 20;
        this.bodyImg.regX +=img.getBounds().width/2;
     
       var fixDef = new Box2D.Dynamics.b2FixtureDef;
       fixDef.density = 1.0;
       fixDef.friction = 0.3;
       fixDef.restitution = 0.2;
     
     
       var bodyDef = new box2d.b2BodyDef;

       bodyDef.type = type;
       bodyDef.fixedRotation = true;
       bodyDef.position.x = pX / scale;
       bodyDef.position.y = pY / scale;

       fixDef.shape = new box2d.b2PolygonShape();
     
       fixDef.shape.SetAsBox((width / scale) / 2, (height)/ scale /2);

       this.bodyImg.body = world.CreateBody(bodyDef);
       this.bodyImg.body.SetUserData(this);
       this.bodyImg.body.SetLinearDamping(0.45);
        this.bodyImg.body.width = width / scale;
          this.bodyImg.body.height = height / scale;


       this.bodyImg.body.CreateFixture(fixDef);

      this.update = function () {
            this.bodyImg.x = this.bodyImg.body.GetPosition().x * scale;
            this.bodyImg.y = this.bodyImg.body.GetPosition().y * scale;
            this.bodyImg.rotation = this.bodyImg.body.GetAngle() * (180 / Math.PI);
      }    
           this.contact = function(contact, impulse, first) {
               if(contact.blockType == blockType.SPIKE_BLOCK) {
                    console.log("die");   
               }
      }
 
          
}

function updatePhysics(dt) {
    
    world.Step(dt,10, 10);

 
    
//    world.DrawDebugData();
    world.ClearForces();

    
}
